For more than 30 years, Salyer Hearing Center has established the regional gold standard for hearing evaluations, fittings and service. Our audiologists utilize state of the art technology in assessing and treating hearing loss.

Address: 93 Family Church Rd, Suite A, Murphy, NC 28906, USA

Phone: 828-835-1014
